<div class="container-fluid">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <h1>Dashboard</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <?php
            include "../templates/sidebar.view.php"
            ?>
        </div>
        <div class="col-sm-10">
            <div class="well">
                <p class="lead">
                    <?= count($contributions) ?> Contributions up to now
                </p>
            </div>
        </div>
    </div>
</div>