<div class="container-fluid">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <h1>Contributions</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <?php
            include "../templates/sidebar.view.php"
            ?>

            <h2>Export</h2>

            <a class="btn btn-success btn-lg btn-block" data-toggle="modal" href='#modal-id1'>Download Data</a>
            <div class="modal fade" id="modal-id1">
                <div class="modal-dialog">
                    <form method="POST" action="/dashboard/download.php">

                        <input type="hidden" name="export_all_data" value="export_all_data">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Download</h4>
                            </div>
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="from">From</label>
                                    <input type="date" name="from" id="from" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="to">To</label>
                                    <input type="date" name="to" id="to" class="form-control">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">
                                    Download
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <br>

            <a class="btn btn-success btn-lg btn-block" data-toggle="modal" href='#modal-id2'>Download Names</a>
            <div class="modal fade" id="modal-id2">
                <div class="modal-dialog">
                    <form method="POST" action="/dashboard/download.php">

                        <input type="hidden" name="export_name_list" value="export_name_list">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Download</h4>
                            </div>
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="from">From</label>
                                    <input type="date" name="from" id="from" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="to">To</label>
                                    <input type="date" name="to" id="to" class="form-control">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">
                                    Download
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <br><br>

        </div>
        <div class="col-sm-10">
            <?php if (count($contributions) > 0): ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th class="text-right">Amount (Tshs)</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($contributions as $contribution): ?>
                            <tr>
                                <td class="text-right"><?= $i++; ?>.</td>
                                <td><?= $contribution['name'] ?></td>
                                <td><?= $contribution['phone'] ?></td>
                                <td class="text-right"><?= number_format($contribution['amount']) ?></td>
                                <td><?= $contribution['contributed_at'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php else: ?>
                <div class="alert alert-info">
                    <p class="lead">
                        No Data
                    </p>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>