<?php

require_once '../middleware/auth.php';
require_once "../vendor/autoload.php";
require_once '../config/database.php';

$pdf = new FPDF();
$pdf->addPage();
$pdf->setFont('Arial', 'B', 16);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['export_name_list']) && $_POST['export_name_list'] == 'export_name_list') {
        $sqlNames = "SELECT name FROM contributions;";
        $stmtNames = $conn->prepare($sqlNames);
        $stmtNames->execute();

        $names = $stmtNames->fetchAll(PDO::FETCH_ASSOC);

//        $columnHeadings = ['Name'];

        $pdf->Cell(20, 10, 'Majina Yote');


//        // PRINT HEADERS
//        foreach ($columnHeadings as $columnHeading) {
//            $pdf->Cell(190, 12, $columnHeading, 1);
//        }

        // PRINT DATA
        foreach ($names as $name) {
            $pdf->setFont('Arial', '', 12);
            $pdf->Ln();
            foreach ($name as $column) {
                $pdf->Cell(190, 12, $column, 1);
            }
        }

    }

}


if ($_SERVER['REQUEST_METHOD'] == 'POST' AND isset($_POST['export_all_data'])) {

    // GET FORM DATA
    $from = $_POST['from'];
    $to = $_POST['to'];

    $pdf->Cell(20, 10, 'Majina ya Waliochangia  Tarehe ' . $from);
    $pdf->Ln();


    // COLUMN HEADINGS
    $columnHeadings = ['JINA', 'SIMU', 'KIASI (Tshs)', 'TAREHE'];

    // DATA
    $sql2 = "SELECT name, phone, amount, DATE(contributed_at) FROM contributions WHERE contributed_at >=:from AND contributed_at <=:to;";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->bindParam(':from', $from);
    $stmt2->bindParam(':to', $to);
    $stmt2->execute();

    $contributions = $stmt2->fetchAll(PDO::FETCH_ASSOC);


    // PRINT HEADERS
    foreach ($columnHeadings as $columnHeading) {
        $pdf->Cell(45, 12, $columnHeading, 1);
    }

    // PRINT DATA
    foreach ($contributions as $contribution) {
        $pdf->setFont('Arial', '', 12);
        $pdf->Ln();
        foreach ($contribution as $column) {
            $pdf->Cell(45, 12, $column, 1);
        }
    }

}

$pdf->Output();


