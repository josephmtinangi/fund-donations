<?php

require_once '../middleware/auth.php';
require_once '../config/database.php';


$sql = "SELECT * FROM contributions;";
$stmt = $conn->prepare($sql);
$stmt->execute();

$contributions = $stmt->fetchAll(PDO::FETCH_ASSOC);

$title = "Dashboard";

include "../templates/header.view.php";

include "index.view.php";

include "../templates/footer.view.php";
