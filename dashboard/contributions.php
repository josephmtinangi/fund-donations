<?php

require_once '../middleware/auth.php';
require_once '../config/database.php';


$sql = "SELECT name, phone, amount, DATE(contributed_at) contributed_at FROM contributions;";
$stmt = $conn->prepare($sql);
$stmt->execute();

$contributions = $stmt->fetchAll(PDO::FETCH_ASSOC);

$title = "Contributions";

include "../templates/header.view.php";

include "contributions.view.php";

include "../templates/footer.view.php";
