<?php

require_once '../middleware/auth.php';
require_once "../vendor/autoload.php";
require_once '../config/database.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST' AND isset($_POST['export_all_data'])) {

    // GET FORM DATA
    $from = $_POST['from'];
    $to = $_POST['to'];

    // DATA
    $sql2 = "SELECT name as Jina, phone as Simu, amount as Kiasi, DATE(contributed_at) as Tarehe FROM contributions WHERE contributed_at >=:from AND contributed_at <=:to ORDER BY Jina ASC;";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->bindParam(':from', $from);
    $stmt2->bindParam(':to', $to);
    $stmt2->execute();

    $contributions = $stmt2->fetchAll(PDO::FETCH_ASSOC);

    // var_dump($contributions);die);
    exportExcel($contributions, $from, $to);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['export_name_list']) && $_POST['export_name_list'] == 'export_name_list') {

	    // GET FORM DATA
	    $from = $_POST['from'];
	    $to = $_POST['to'];

        $sqlNames = "SELECT name as Jina FROM contributions WHERE contributed_at >=:from AND contributed_at <=:to ORDER BY Jina ASC;";
        $stmtNames = $conn->prepare($sqlNames);
	    $stmtNames = $conn->prepare($sqlNames);
	    $stmtNames->bindParam(':from', $from);
	    $stmtNames->bindParam(':to', $to);        
        $stmtNames->execute();

        $names = $stmtNames->fetchAll(PDO::FETCH_ASSOC);

        // var_dump($names);die();
        if(count($names) == 0) {
        	echo '0 data found.';
        	die();
        }

        exportNames($names, $from, $to);
    }

}

function exportExcel($data, $from, $to) {
	$phpExcel = new PHPExcel();

	$filename = "Contributions from " . $from . " to " . $to . ".xlsx";

	$phpExcel->getProperties()->setCreator("Admin")
								 ->setTitle("Contributions from " . $from . " to " . $to);

	$phpExcel->setActiveSheetIndex(0)
			 ->setCellValue('A1', 'Jina')
			 ->setCellValue('B1', 'Simu')
			 ->setCellValue('C1', 'Kiasi')
			 ->setCellValue('D1', 'Tarehe');	

	$j = 2;
	for($i = 0; $i < count($data); $i++) {
		$phpExcel->setActiveSheetIndex(0)
				 ->setCellValue('A' . $j, $data[$i]['Jina'])
				 ->setCellValue('B' . $j, $data[$i]['Simu'])
				 ->setCellValue('C' . $j, $data[$i]['Kiasi'])
				 ->setCellValue('D' . $j, $data[$i]['Tarehe']);	
		$j += 1;
	}

	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="' . $filename . '"');
	header('Cache-Control: max-age=0');
	header('Cache-Control: max-age=1');

	$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;	
}

function exportNames($data, $from, $to) {
	$phpExcel = new PHPExcel();

	$filename = "Contributions from " . $from . " to " . $to . ".xlsx";	

	$phpExcel->getProperties()->setCreator("Admin")
							  ->setTitle("Contributions from " . $from . " to " . $to);

	$phpExcel->setActiveSheetIndex(0)
			 ->setCellValue('A1', 'Jina');	

	$j = 2;
	for($i = 0; $i < count($data); $i++) {
		$phpExcel->setActiveSheetIndex(0)
				 ->setCellValue('A' . $j, $data[$i]['Jina']);	
		$j += 1;
	}

	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="' . $filename . '"');
	header('Cache-Control: max-age=0');
	header('Cache-Control: max-age=1');

	$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;	
}
