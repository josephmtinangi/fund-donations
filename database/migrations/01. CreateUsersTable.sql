CREATE TABLE users (
  id INT (11) PRIMARY KEY auto_increment,
  name VARCHAR (255) NULL,
  email VARCHAR (255),
  password VARCHAR (255),
  contributed_at TIMESTAMP ,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
