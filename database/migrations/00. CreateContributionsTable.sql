CREATE TABLE contributions (
  id INT (11) PRIMARY KEY auto_increment,
  name VARCHAR (255),
  phone VARCHAR (255),
  amount DOUBLE,
  contributed_at TIMESTAMP ,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
