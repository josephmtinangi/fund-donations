<?php

if (isset($_SESSION['email'])) {
	header('Location: /');
}

require_once '../config/database.php';

$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	if (!isset($_POST['email'])) {
		$errors[] = 'Email is required.';
	}

	if (!isset($_POST['password'])) {
		$errors[] = 'Password is required.';
	}	

	$email = $_POST['email'];
	$password = md5($_POST['password']);

	$sql = "SELECT * FROM users WHERE email=:email AND password=:password;";
	$stmt = $conn->prepare($sql);
    $stmt->bindParam(':email', $email);	
    $stmt->bindParam(':password', $password);	
	$stmt->execute();

	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if (count($result) == 1) {
		session_start();
		$_SESSION['email'] = $email;

		header('Location: /dashboard/');
	} else {
		$errors[] = 'Invalid email and/or password';
	}
}

$title = 'Login';

include "../templates/header.view.php";

include 'login.view.php';

include "../templates/footer.view.php";
