<div class="container">
	<div class="row">
		<div class="col-sm-4 col-sm-offset-4">
			<h2>Login</h2>
			<div class="well">

				<?php if(count($errors) > 0): ?>
					<div class="alert alert-danger">
						<ul>
							<?php foreach($errors as $error): ?>
								<li><?=$error?></li>
							<?php endforeach; ?>
						</ul>
					</div>
				<?php endif; ?>

				<form method="POST" action="/auth/login.php">
					
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control" required="required" placeholder="Email">
					</div>

					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" id="password" class="form-control" required="required" placeholder="Password">
					</div>

					<button type="submit" class="btn btn-primary">Login</button>
				</form>
			</div>
		</div>
	</div>
</div>