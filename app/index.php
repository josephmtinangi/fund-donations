<?php

session_start();
require_once '../config/database.php';

$messages = [];
$errors = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {


    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $amount = $_POST['amount'];
    $contributed_at = $_POST['contributed_at'];

    $sql = "INSERT INTO contributions (name, phone, amount, contributed_at) VALUE (:name, :phone, :amount, :contributed_at)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':amount', $amount);
    $stmt->bindParam(':contributed_at', $contributed_at);

    $stmt->execute();

    $messages[] = 'Imefanikiwa kuhifadhiwa!';
}

$title = "Home";

include "../templates/header.view.php";

include "index.view.php";

include "../templates/footer.view.php";
