<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">

                <?php if(count($messages) > 0): ?>
                    <div class="alert alert-success">
                        <ul class="list-unstyled">
                            <?php foreach($messages as $message): ?>
                                <li><?=$message?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>

            <form action="" method="POST" role="form">

                <div class="form-group">
                    <label for="name">Jina</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Jina">
                </div>

                <div class="form-group">
                    <label for="phone">Namba ya Simu</label>
                    <input type="tel" class="form-control" name="phone" id="phone" placeholder="Namba ya Simu">
                </div>

                <div class="form-group">
                    <label for="amount">Kiasi (Tshs)</label>
                    <input type="number" class="form-control" name="amount" id="amount" placeholder="Kiasi">
                </div>

                <div class="form-group">
                    <label for="contributed_at">Tarehe</label>
                    <input type="date" class="form-control" name="contributed_at" id="contributed_at">
                </div>


                <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> OK</button>
            </form>
        </div>
    </div>
</div>